import { OAuthclientModel } from '../../src/db/models/oauthclient.model';
import { User } from '../../src/modules/users/user';
import { UserGateway } from '../../src/modules/users/user-gateway';

export class DBHelper {
    async createOAuthClient(clientId: string, clientSecret: string) {
        const modelValues = {
            name: 'test',
            clientId,
            clientSecret,
            allowedGrants: ['password', 'refresh_token'],
            createdAt: Date.now(),
            updatedAt: Date.now(),
        }

        const created = await OAuthclientModel.create(modelValues);
        return created;
    }

    async createUser(email: string, password: string) {
        const user = new User();
        user.setEmail(email);
        user.setPassword(password);
        const userGateway = new UserGateway();
        const created = await userGateway.createUser(user);
        return created;
    }
}