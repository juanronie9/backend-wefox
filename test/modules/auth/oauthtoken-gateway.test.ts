// @ts-ignore
import mongoose from 'mongoose';
import faker from 'faker';

import { MongoConnector } from '../../../src/modules/common/mongo-connector';
import applicationConfig from '../../../src/config/application';
import { OAuthtokenGateway } from '../../../src/modules/auth/oauthtoken-gateway';
import { OAuthToken } from '../../../src/modules/auth/oauthtoken';
import { OAuthtokenModel } from '../../../src/db/models/oauthtoken.model';

describe('AccessTokenGateway', () => {
    beforeAll(async () => {
        const connector = new MongoConnector();
        connector.setConfig(applicationConfig.mongoDb);
        await connector.connect();
    });
    afterAll(async () => {
        // await db.close()
    });

    describe('createOAuthToken', () => {
        it('should create oauth token', async () => {
            const oauthTokenGateway = new OAuthtokenGateway();

            const oauthClientIdBefore = new mongoose.Types.ObjectId();
            const userIdBefore = new mongoose.Types.ObjectId();
            const accessTokenBefore = faker.datatype.uuid();
            const accessTokenExpiresAtBefore = new Date();

            const oauthToken = new OAuthToken();
            oauthToken.setOAuthClientId(oauthClientIdBefore);
            oauthToken.setUserId(userIdBefore);
            oauthToken.setAccessToken(accessTokenBefore);
            oauthToken.setAccessTokenExpiresAt(accessTokenExpiresAtBefore);

            const response = await oauthTokenGateway.createOAuthToken(oauthToken);
            expect(response).not.toBeNull();
            expect(response.id).toBeDefined();
            expect(response.oauthClientId).toBe(oauthClientIdBefore);
            expect(response.userId).toBe(userIdBefore);
            expect(response.accessToken).toBe(accessTokenBefore);
            expect(response.accessTokenExpiresAt).toBe(accessTokenExpiresAtBefore)

            const data = await OAuthtokenModel.findById(response.id).exec();
            expect(data).toBeDefined();
        });
    });

    describe('validateAccessToken', () => {
        it('should not return data: not found', async () => {
            const oauthTokenGateway = new OAuthtokenGateway();

            const data = await oauthTokenGateway.validateAccessToken(faker.datatype.uuid());
            expect(data).toBeNull();
        });

        it('should return data', async () => {
            const oauthTokenGateway = new OAuthtokenGateway();

            const oauthClientIdBefore = new mongoose.Types.ObjectId();
            const userIdBefore = new mongoose.Types.ObjectId();
            const accessTokenBefore = faker.datatype.uuid();
            const accessTokenExpiresAtBefore = new Date();
            accessTokenExpiresAtBefore.setSeconds( accessTokenExpiresAtBefore.getSeconds() + 10*60*60);

            const oauthToken = new OAuthToken();
            oauthToken.setOAuthClientId(oauthClientIdBefore);
            oauthToken.setUserId(userIdBefore);
            oauthToken.setAccessToken(accessTokenBefore);
            oauthToken.setAccessTokenExpiresAt(accessTokenExpiresAtBefore);

            await oauthTokenGateway.createOAuthToken(oauthToken);

            const data = await oauthTokenGateway.validateAccessToken(accessTokenBefore);
            expect(data.accessToken).toBe(accessTokenBefore);
        });
    });
});