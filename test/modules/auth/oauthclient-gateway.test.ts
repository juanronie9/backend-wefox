import faker from 'faker';
import { MongoConnector } from '../../../src/modules/common/mongo-connector';
import applicationConfig from '../../../src/config/application';
import { OAuthClientGateway } from '../../../src/modules/auth/oauthclient-gateway';
import { OAuthclientModel } from '../../../src/db/models/oauthclient.model';

describe('OAuthClientGatway', () => {
    beforeAll(async () => {
        const connector = new MongoConnector();
        connector.setConfig(applicationConfig.mongoDb);
        await connector.connect();
    })

    afterAll(async () => {
        // await db.close()
    })

    describe('validateOAuthClient', () => {
        it('should not return data: not found', async () => {
            const oauthClientGateway = new OAuthClientGateway();

            const data = await oauthClientGateway.validateOAuthClient(faker.datatype.uuid(), faker.datatype.uuid(), 'password');
            expect(data).toBeNull();
        });

        it('should return data', async () => {
            const modelValues = {
                name: 'test',
                clientId: faker.datatype.uuid(),
                clientSecret: faker.datatype.uuid(),
                redirectUris: [faker.internet.url],
                allowedGrants: ['password'],
            }
            await OAuthclientModel.create(modelValues);

            const oauthClientGateway = new OAuthClientGateway();
            const data = await oauthClientGateway.validateOAuthClient(
                modelValues.clientId,
                modelValues.clientSecret,
                'password',
            );
            expect(data._id).toBeDefined();
        });
    });
});