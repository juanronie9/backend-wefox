import { IGetAddressRequestParameters, MapsService } from '../../../src/modules/maps/maps-service';

describe('MapsService', () => {
    describe('getAddress', () => {
        it('should not return data: not found', async () => {
            const mapsService = new MapsService();
            const obj: IGetAddressRequestParameters = {
                street: '---',
                streetNumber: '---',
                town: '---',
                postalCode: '---',
                country: '---'
            }
            const data = await mapsService.getAddress(obj);
            expect(data).toBeNull();
        });
        it('should return data', async () => {
            const mapsService = new MapsService();
            const obj: IGetAddressRequestParameters = {
                street: 'Alcalá',
                streetNumber: '42',
                town: 'Madrid',
                postalCode: '28014',
                country: 'Spain'
            }
            const data = await mapsService.getAddress(obj);
            expect(data).toBeDefined();
            expect(data).toHaveProperty('lat');
            expect(data).toHaveProperty('lon');
        });
    });

    describe('getWeather', () => {
        it('should not return data: not found', async () => {
            try {
                const mapsService = new MapsService();
                await mapsService.getWeather('---','-3.867118');
            } catch (e) {
                expect(e.message).toBe('Unable to obtain weather. ErrorCode: 400, ErrorMessage: wrong latitude');
            }
        });
        it('should not return data: not found', async () => {
            try {
                const mapsService = new MapsService();
                await mapsService.getWeather('40.3300964','---');
            } catch (e) {
                expect(e.message).toBe('Unable to obtain weather. ErrorCode: 400, ErrorMessage: wrong longitude');
            }
        });
        it('should return data', async () => {
            const mapsService = new MapsService();
            const data = await mapsService.getWeather('40.3300964', '-3.867118');
            expect(data).toBeDefined();
            expect(data.weather).toBeDefined();
        });
    });
});