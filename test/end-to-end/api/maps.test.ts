import supertest from 'supertest';
import _ from 'lodash';
import { MongoConnector } from '../../../src/modules/common/mongo-connector';
import applicationConfig from '../../../src/config/application';
import { DBHelper } from '../../helpers/dbHelper';

const prefix = '/v1/';

function api(path) {
    return prefix + path;
}

let request;

const bodyRequest = {
    street: 'Alcalá',
    streetNumber: '42',
    town: 'Madrid',
    postalCode: '28014',
    country: 'spain'
}
const bodyRequestAuth = {
    client_id: '2_3nio9v1agigw844kssgk4kswksgs0wk4okkoksg4080osoos84',
    client_secret: '4394ff69idq8g08k4wso4ck8008w0g44ck88wo8c8cw80wk8c4',
    grant_type: 'password',
    username: 'test@test.com',
    password: 'Asdf1234'
};
const credentials = {
    'access-token': '',
};

// NOTE: Simulated data, in a real application use this flow for every test:
// 1. Insert data in db
// 2. Get data from db
// 3. Verify data from db
// 4. Clear data from db

describe('MapsApiTest', () => {
    beforeAll(async () => {
        const connector = new MongoConnector();
        connector.setConfig(applicationConfig.mongoDb);
        await connector.connect();

        // INIT DATA
        const dbHelper = new DBHelper();
        await dbHelper.createOAuthClient(bodyRequestAuth.client_id, bodyRequestAuth.client_secret);
        await dbHelper.createUser(bodyRequestAuth.username, bodyRequestAuth.password);

        request = supertest('http://0.0.0.0:3000');
        const response = await request
            .post(api('oauth/token'))
            .send(bodyRequestAuth);

        credentials['access-token'] = response.body.access_token;
    });
    afterAll(async () => {
    });

    describe('Get Maps [POST] /v1/maps', () => {
        it('should fail when body is not provided', async () => {
            const response = await request
                .post(api('maps'))
                .send({});

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when body is not valid: parameters required missing', async () => {
            const bodyRequestTest = _.clone(bodyRequest);
            delete bodyRequestTest.country;
            const response = await request
                .post(api('maps'))
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when body is not valid: parameters type not valid', async () => {
            const bodyRequestTest = _.clone(bodyRequest);
            bodyRequestTest.country = 1234;
            const response = await request
                .post(api('maps'))
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when data is not valid: country', async () => {
            const bodyRequestTest = _.clone(bodyRequest);
            bodyRequestTest.country = 'value_not_valid';
            const response = await request
                .post(api('maps'))
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(404);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should success', async () => {
            const bodyRequestTest = _.clone(bodyRequest);
            const response = await request
                .post(api('maps'))
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(200);
            const { body } = response;
            expect(body.lat).not.toBeNull();
            expect(body.lon).not.toBeNull();
        });
    });

    describe('Get Weather [POST] /v1/maps/weather', () => {
        it('should fail when credentials not provided', async () => {
            const response = await request
                .post(api('maps/weather'));

            expect(response.statusCode).toBe(401);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when credentials not valid', async () => {
            const badCredentials = _.clone(credentials);
            badCredentials['access-token'] = '------';
            const response = await request
                .post(api('maps/weather'))
                .set(badCredentials);

            expect(response.statusCode).toBe(401);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when body is not provided', async () => {
            const response = await request
                .post(api('maps/weather'))
                .set(credentials)
                .send({});

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when body is not valid: parameters required missing', async () => {
            const bodyRequestTest = _.clone(bodyRequest);
            delete bodyRequestTest.country;
            const response = await request
                .post(api('maps/weather'))
                .set(credentials)
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when body is not valid: parameters type not valid', async () => {
            const bodyRequestTest = _.clone(bodyRequest);
            bodyRequestTest.country = 1234;
            const response = await request
                .post(api('maps/weather'))
                .set(credentials)
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when data is not valid: country', async () => {
            const bodyRequestTest = _.clone(bodyRequest);
            bodyRequestTest.country = 'value_not_valid';
            const response = await request
                .post(api('maps/weather'))
                .set(credentials)
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(404);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should success', async () => {
            const bodyRequestTest = _.clone(bodyRequest);
            const response = await request
                .post(api('maps/weather'))
                .set(credentials)
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(200);
            const { body } = response;
            expect(body.weather).toBeDefined();
        });
    });
});
