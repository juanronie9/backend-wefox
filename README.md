# The Backend API

Provides a rich `backend-api` abilities.

## API

API is built on top of [OpenAPI 3.0 specification](https://swagger.io/specification/)
See: [swagger.yml](./src/templates/api-docs/swagger.yaml) for details.

#### Documentation
```
# DEV
http://localhost:3000/docs/api

# PROD
https://<domain>/docs/api
```

## Building

The project uses [TypeScript](https://github.com/Microsoft/TypeScript) as a JavaScript transpiler.
The sources are located under the `src` directory, the distributables are in the `dist`.

### Requirements:

-   Nodejs
-   MongoDB
-   Redis

### Environment:

```bash
APP_HTTP_PORT=3000
APP_PUBLIC_URL=http://localhost:3000
APP_ENVIRONMENT_NAME=
APP_MONGO_DATABASE=
APP_MONGO_HOST=
MONGO_PASS=
MONGO_PORT=27017
MONGO_SRV=
APP_MONGO_USER=
APP_ADMIN_ACCESS_IP_LIST=
API_WEATHER_KEY=
APP_REDIS_HOST=
APP_REDIS_PORT=6379
```

To make the application running use

```bash
npm run build
npm run start
```


## Development

```bash
npm run debug
```

## Testing

It uses Jest and supertest.
```bash
npm run test
```
Open one terminal with project running and other terminal to execute the e2e api tests.
```bash
npm run test:e2e
```
---

## Docker
Init
```
docker-compose up -d
```
Execute tests 
```
docker-compose exec api npm run test:e2e
docker-compose exec api npm run test
```
Execute single service
```
docker-compose up -d redis
docker-compose up -d mongo
```
Rebuild if some change 
```
docker-compose down -v
docker-compose build --no-cache
docker-compose up -d
```


## Application Structure
- [src](./src): The main `backend` application code.
    - [api](./src/api): Defines API routers and controllers
    - [config](./src/config): Defines configuration. Examples ENV_VARS
    - [db](./src/db): MongoDB models and schemas
    - [modules](./src/modules): Defines modules and logic of the application. 
      All the modules are composed of `Interactors` used by controllers, `Gateways` to interact with DB and `Services` to handle logic or request external REST API's.   
- [test](./test): Includes all the testing of the application
- [app.ts](./app.ts): Main file. Inject packages, initiate and bootstrap application


---

# Next Steps (improvements)
- Develop generic solution to integrate different federated Identities using oauth
- Update tokens to JWT
- Improve tests, documentation 
- Increase available APIs and functionalities

# OAuth Grant Types used

### OAuth Resource Owner Password Flow 
For users registered in our database with username and password. Allows exchanging the usename and password for an access token.
```
curl -X POST \
  http://0.0.0.0:3000/v1/oauth/token \
  -H 'content-type: application/json' \
  -d '{
    "client_id": "CLIENT_ID",
    "client_secret": "CLIENT_SECRET",
    "grant_type": "password"
    "username": "USER_EMAIL",
    "password: "USER_PASSWORD"
}'
```
```
{
    "access_token": "7a2b2594-c0ae-4ace-8f12-2c885961dbb6",
    "expires_in": 36000,
    "refresh_token": "c0c19e43-7bed-4289-93f2-01d1f71523c4"
}
```
## OAuth Refresh Token Flow
Allows exchanging the refresh tokens for an access token.
```
curl -X POST \
  http://0.0.0.0:3000/v1/oauth/token \
  -H 'content-type: application/json' \
  -d '{
    "client_id": "CLIENT_ID",
    "client_secret": "CLIENT_SECRET",
    "grant_type": "refresh_token"
    "refresh_token": "REFRESH_TOKEN",
}'
```
```
{
    "access_token": "7a2b2594-c0ae-4ace-8f12-2c885961dbb6",
    "expires_in": 36000,
    "refresh_token": "c0c19e43-7bed-4289-93f2-01d1f71523c4"
}
```

---

## EXTRA: How to integrate third party federated identities?
### OAuth Authorization Code Flow

The federated identity for this example is **Github**.

How to integrate third party federated identities? Steps:

- The application opens a browser to send the user to the OAuth server
- The user sees the authorization prompt and approves the app’s request
- The user is redirected back to the application with an authorization code in the query string
- The application exchanges the authorization code for an access token

Note: Pre-register your oauth application in the identity provider with redirect URIs.

##### Authorization
1. Authorization Code Link: will be in the frontend button link. Example:
    ```
    http://github.com/login/oauth/authorize?client_id=CLIENT_ID&redirect_uri=REDIRECT_URI&scope=SCOPE&state=STATE
    ```
    ```
    <a href="http://github.com/login/oauth/authorize?client_id=CLIENT_ID&redirect_uri=REDIRECT_URI&scope=SCOPE&state=STATE" class="btn btn-danger"><span class="fa fa-github"></span> Github Login</a>
    ```

2. User Authorizes Application: When the user clicks the link, they must first log in to the service to authenticate their identity (unless they are already logged in). Then they will be prompted by the service to authorize or deny the application access to their account.


3. Application Receives Authorization Code: If the user clicks "Allow," the service redirects the user back to your site with an authorization code.
    ```
    https://REDIRECT_URI_HERE?code=AUTH_CODE_HERE&state=1234zyx
    ```
    - code - The server returns the authorization code in the query string
    - state - The server returns the same state value that you passed
    ```
    http://0.0.0.0:3000/v1/oauth/github/callback?code=1234
    ```

#### Getting an Access Token
Exchange the authorization code for an access token by making a POST request to the authorization server's token endpoint
```
curl -X POST \
  https://github.com/login/oauth/access_token \
  -H 'content-type: application/json' \
  -H 'x-user-id: 60def766e9a13e0007e09f50' \
  -d '{
    "code": "AUTH_CODE",
    "client_id": "CLIENT_ID",
    "client_secret": "CLIENT_SECRET"
}'
```
```
{
    "access_token": "7a2b2594-c0ae-4ace-8f12-2c885961dbb6",
    "expires_in": 36000,
    "refresh_token": "c0c19e43-7bed-4289-93f2-01d1f71523c4"
}
```
#### Get User Info
Request the API with an access_token to get userinfo
```
curl -X GET \
  https://api.github.com/user \
  -H 'content-type: application/json' \
  -H 'Authorization: token ACCESS_TOKEN_HERE'
```