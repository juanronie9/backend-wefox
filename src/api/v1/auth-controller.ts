import { ExpressController, ExpressJSONShowPresenter } from '../../modules/core';
import { OAuthClientGateway } from '../../modules/auth/oauthclient-gateway';
import { GetOauthtokenInteractor,
    GetOAuthTokenRequestObject,
    validationSchema as createTokensSchema
} from '../../modules/auth/get-oauthtoken-interactor';
import { OAuthtokenGateway } from '../../modules/auth/oauthtoken-gateway';
import { OauthrefreshtokenGateway } from '../../modules/auth/oauthrefreshtoken-gateway';

export class AuthController extends ExpressController {
    constructor() {
        super();

        this.router.post('/token', this.validator.validateBody(createTokensSchema), this.getOAuthToken.bind(this));
    }

    async getOAuthToken(req, res, next) {
        const oauthClientGateway = new OAuthClientGateway();
        const oauthTokenGateway = new OAuthtokenGateway();
        const oauthRefreshTokenGateway = new OauthrefreshtokenGateway();

        const interactor = new GetOauthtokenInteractor();

        const presenter = new ExpressJSONShowPresenter(req, res, next);

        const request = req.body as GetOAuthTokenRequestObject;

        interactor.setPresenter(presenter)
            .setOAuthTokenGateway(oauthTokenGateway)
            .setOAuthClientGateway(oauthClientGateway)
            .setOAuthRefreshTokenGateway(oauthRefreshTokenGateway);

        await interactor.execute(request);
    }
}
