import { Router } from 'express';

import { AuthController } from './auth-controller';
import { MapsController } from './maps-controller';

export class ApiRouter {

    constructor() {}

    build() {
        const router = Router();

        const authController = new AuthController();
        const mapsController = new MapsController();

        router.get('/healthcheck', (req, res) => {
            const healthcheck = {
                uptime: process.uptime(),
                message: 'OK',
                timestamp: Date.now()
            };
            try {
                return res.status(200).send(healthcheck);
            } catch (e) {
                healthcheck.message = e;
                return res.status(503).send();
            }
        });

        // OAuth api
        router.use('/oauth', authController.router);

        // Maps api
        router.use('/maps', mapsController.router);

        return router;
    }
}
