import { ExpressController, ExpressJSONShowPresenter } from '../../modules/core';
import { GetMapsInteractor, GetMapsRequestObject, validationSchema as createMapsSchema } from '../../modules/maps/get-maps-interactor';
import { GetWeatherInteractor, GetWeatherRequestObject, validationSchema as createWeatherSchema } from '../../modules/maps/get-weather-interactor';
import { AuthMiddleware } from '../../modules/core/auth.middleware';

export class MapsController extends ExpressController {
    constructor() {
        super();

        this.router.post('/', this.validator.validateBody(createMapsSchema), this.getMaps.bind(this));
        this.router.post('/weather', AuthMiddleware, this.validator.validateBody(createWeatherSchema), this.getWeather.bind(this));
    }

    async getMaps(req, res, next) {
        const interactor = new GetMapsInteractor();

        const presenter = new ExpressJSONShowPresenter(req, res, next);

        const request = req.body as GetMapsRequestObject;

        interactor.setPresenter(presenter);

        await interactor.execute(request);
    }

    async getWeather(req, res, next) {
        const interactor = new GetWeatherInteractor();

        const presenter = new ExpressJSONShowPresenter(req, res, next);

        const request = req.body as GetWeatherRequestObject;

        interactor.setPresenter(presenter);

        await interactor.execute(request);
    }
}
