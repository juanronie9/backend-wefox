import { logger } from '../../app';
import axios from 'axios';
import applicationConfig from '../../config/application';
import { RedisStorage } from '../common/redis-storage';

const DEFAULT_CACHE_TIME_MS = 12 * 60 * 60 * 1000; // 12h

export interface IGetAddressRequestParameters {
    street: string,
    streetNumber: string,
    town: string;
    postalCode: string;
    country: string;
}

export interface IGetAddressResponse {
    name: string,
    lat: string,
    lon: string,
    type: string,
}

export interface IGetWeatherResponse {
    weather: any,
    temp: number,
    temp_min: number,
    temp_max: number,
    pressure: number,
    humidity: number
}

export class MapsService {

    private _redisStorage;

    constructor() {
        this._redisStorage = RedisStorage.getInstance();
    }

    async getAddress({ street, streetNumber, town, postalCode, country }: IGetAddressRequestParameters): Promise<IGetAddressResponse> {
        const key = `${country}_${postalCode}_${town}_${street}_${streetNumber}`;
        const base64key = Buffer.from(key).toString('base64');
        const cachedAddress: IGetAddressResponse = await this._redisStorage.getObject(`bw:address:${base64key}`);
        if (cachedAddress) {
            return cachedAddress;
        }

        const uri = `https://nominatim.openstreetmap.org/search`;
        const options = {
            method: 'GET',
            uri,
            params: {
                'street': `${ streetNumber } ${ street }`,
                'city': town,
                'country': country,
                'postalcode': postalCode,
                'format': 'json'
            },
            json: true,
        };

        try {
            const result = await axios.get(options.uri, { params: options.params });
            if (result.data.length === 0) {
                return null;
            }
            const data = result.data[0];
            const obj: IGetAddressResponse = {
                name: data.display_name,
                lat: data.lat,
                lon: data.lon,
                type: data.type
            }

            await this._redisStorage.setObject(`bw:address:${base64key}`, obj, DEFAULT_CACHE_TIME_MS);

            return obj;
        } catch (error) {
            logger.error('Error during address obtaining: ', error.status, error.message);
            throw new Error(`Unable to obtain address. ErrorCode: ${error.status}`);
        }
    }

    async getWeather(lat: string, lon: string): Promise<IGetWeatherResponse> {
        const key = `${lat}_${lon}`;
        const base64key = Buffer.from(key).toString('base64');
        const cachedWeather: IGetWeatherResponse = await this._redisStorage.getObject(`bw:weather:${base64key}`);
        if (cachedWeather) {
            return cachedWeather;
        }

        const uri = `https://api.openweathermap.org/data/2.5/weather`;
        const options = {
            method: 'GET',
            uri,
            params: {
                'lat': lat,
                'lon': lon,
                'units': 'metric',
                'appId': applicationConfig.weatherProvider.key
            },
            json: true,
        };

        try {
            const result = await axios.get(options.uri, { params: options.params });
            const data = result.data;
            const obj: IGetWeatherResponse = {
                weather: data.weather,
                temp: data.main.temp,
                temp_min: data.main.temp_min,
                temp_max: data.main.temp_max,
                pressure: data.main.pressure,
                humidity: data.main.humidity,
            }

            await this._redisStorage.setObject(`bw:weather:${base64key}`, obj, DEFAULT_CACHE_TIME_MS);

            return obj;
        } catch (error) {
            logger.error('Error during weather obtaining: ', error.response.status, error.response.data.message);
            throw new Error(`Unable to obtain weather. ErrorCode: ${error.response.status}, ErrorMessage: ${error.response.data.message}`);
        }
    }
}
