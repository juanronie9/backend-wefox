import { Interactor, IRequestObject } from '../core';
import joi from 'joi';
import { logger } from '../../app';
import { MapsService } from './maps-service';

export interface GetWeatherRequestObject extends IRequestObject {
    street: string,
    streetNumber: string,
    town: string;
    postalCode: string;
    country: string;
}

export const validationSchema = joi.object().keys({
    street: joi
        .string()
        .required(),
    streetNumber: joi
        .string()
        .required(),
    town: joi
        .string()
        .required(),
    postalCode: joi
        .string()
        .required(),
    country: joi
        .string()
        .required(),
});

export class GetWeatherInteractor extends Interactor {

    async execute(request: GetWeatherRequestObject) {
        try {
            const mapsService = new MapsService();
            const data = await mapsService.getAddress(request);
            if (!data) {
                return this.presenter.error(['Address not found']);
            }

            const response = await mapsService.getWeather(data.lat, data.lon);
            if (!response) {
                return this.presenter.error(['Weather not found']);
            }
            return this.presenter.success(response);
        } catch (e) {
            logger.error('Failed to get weather', request, e);
            return this.presenter.error([e.message]);
        }
    }
}