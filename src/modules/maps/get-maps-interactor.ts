import { Interactor, IRequestObject } from '../core';
import joi from 'joi';
import { logger } from '../../app';
import { MapsService } from './maps-service';

export interface GetMapsRequestObject extends IRequestObject {
    street: string,
    streetNumber: string,
    town: string;
    postalCode: string;
    country: string;
}

export const validationSchema = joi.object().keys({
    street: joi
        .string()
        .required(),
    streetNumber: joi
        .string()
        .required(),
    town: joi
        .string()
        .required(),
    postalCode: joi
        .string()
        .required(),
    country: joi
        .string()
        .required(),
});

export class GetMapsInteractor extends Interactor {

    async execute(request: GetMapsRequestObject) {
        try {
            const mapsService = new MapsService();
            const data = await mapsService.getAddress(request);
            if (!data) {
                return this.presenter.error(['Address not found']);
            }
            return this.presenter.success(data);
        } catch (e) {
            logger.error('Failed to validate address', request, e);
            return this.presenter.error([e.message]);
        }
    }
}