import { logger } from '../../app';
const DEFAULT_TTL_MS = 10 * 60 * 1000; // 10 minutes

export class RedisStorage {
    static __instance: RedisStorage;
    _prefix = '';
    _redis;

    static getInstance(): RedisStorage {
        if (!this.__instance) {
            this.__instance = new RedisStorage();
        }
        return this.__instance;
    }

    get redis() {
        return this._redis;
    }

    setRedis(redis) {
        this._redis = redis;
        return this;
    }

    setPrefix(value){
        this._prefix = value;
        return this;
    }

    async getObject(key) {
        key = `${this._prefix}:${key}`;
        try {
            const value = await this.redis.get(key);
            return JSON.parse(value);
        } catch (err) {
            logger.debug('Failed to get object', err);
            return null;
        }
    }

    async setObject(key, value, ttl = DEFAULT_TTL_MS) {
        key = `${this._prefix}:${key}`;
        const pipeline = await this.redis.pipeline();
        pipeline.set(key, JSON.stringify(value));
        pipeline.persist(key);
        pipeline.pexpire(key, ttl);
        await pipeline.exec();
    }

    async deleteKeys(keys: string[]) {
        if(Array.isArray(keys)){
            keys = keys.map(key =>  `${this._prefix}:${key}`);
        }
        return this.redis.del(keys);
    }
}
