import { IPersistableEntity, PersistableEntity } from '../common';
import { OAuthRefreshTokenModel } from '../../db/models/oauthrefreshtoken.model';

export interface IOAuthRefreshToken extends IPersistableEntity {
    refreshToken: string,
    refreshTokenExpiresAt: Date;
    oauthClientId: KeyValueHash;
    userId: KeyValueHash;
}

export class OAuthRefreshToken extends PersistableEntity implements IOAuthRefreshToken {
    private _refreshToken: string;
    private _refreshTokenExpiresAt: Date;
    private _oauthClientId: KeyValueHash;
    private _userId: KeyValueHash;

    get refreshToken(): string {
        return this._refreshToken;
    }

    setRefreshToken(value: string): OAuthRefreshToken {
        this._refreshToken = value;
        return this;
    }

    get refreshTokenExpiresAt(): Date {
        return this._refreshTokenExpiresAt;
    }

    setRefreshTokenExpiresAt(value: Date): OAuthRefreshToken {
        this._refreshTokenExpiresAt = value;
        return this;
    }

    get oauthClientId(): KeyValueHash {
        return this._oauthClientId;
    }

    setOAuthClientId(value): OAuthRefreshToken {
        this._oauthClientId = value;
        return this;
    }

    get userId(): KeyValueHash {
        return this._userId;
    }

    setUserId(value): OAuthRefreshToken {
        this._userId = value;
        return this;
    }

    toJSON() {
        const base = super.toJSON();

        return {
            ...base,
            refreshToken: this._refreshToken,
            refreshTokenExpiresAt: this._refreshTokenExpiresAt,
            oauthClientId: this._oauthClientId,
            userId: this._userId,
        };
    }

    toModelValues() {
        const raw = this.toJSON();

        for (const key of Object.keys(raw)) {
            if (!raw[key]) delete raw[key];
        }
        return raw;
    }

    static fromRaw(
        raw: KeyValueHash | typeof OAuthRefreshTokenModel | any,
        options?: KeyValueHash,
    ): OAuthRefreshToken {
        const instance = new this();

        instance
            .setId(raw.id)
            .setName(raw.name)
            .setCreatedAt(raw.createdAt)
            .setUpdatedAt(raw.updatedAt)
            .setRefreshToken(raw.refreshToken)
            .setRefreshTokenExpiresAt(raw.refreshTokenExpiresAt)
            .setOAuthClientId(raw.oauthClientId)
            .setUserId(raw.userId)

        return instance;
    }
}
