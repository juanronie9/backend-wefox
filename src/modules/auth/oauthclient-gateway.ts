import { IOAuthClientModel, OAuthclientModel } from '../../db/models/oauthclient.model';
import { MongoGateway } from '../common/persistance-gateway';


export class OAuthClientGateway extends MongoGateway {
    private _oauthClientModel = OAuthclientModel;

    setOAuthClientModel(model: typeof OAuthclientModel) {
        this._oauthClientModel = model;
        return this;
    }

    async validateOAuthClient(clientId: string, clientSecret: string, grantType: string): Promise<IOAuthClientModel> {
        const query = {
            clientId,
            clientSecret,
            allowedGrants: grantType
        }

        const data = await this._oauthClientModel
            .findOne(query)
            .exec()

        if (!data) {
            return null;
        }
        return data;
    }
}
