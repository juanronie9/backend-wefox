import { Interactor, IRequestObject } from '../core';
import joi from 'joi';
import { logger } from '../../app';
import { AuthService } from './auth-service';
import { OAuthClientGateway } from './oauthclient-gateway';
import { OAuthtokenGateway } from './oauthtoken-gateway';
import { OAuthToken } from './oauthtoken';
import { v4 as uuidv4 } from 'uuid';
import { OAuthRefreshToken } from './oauthrefreshtoken';
import { OauthrefreshtokenGateway } from './oauthrefreshtoken-gateway';

export interface GetOAuthTokenRequestObject extends IRequestObject {
    grant_type: string,
    client_id: string,
    client_secret: string;
    username: string;
    password: string;
}

export const validationSchema = joi.object().keys({
    grant_type: joi
        .string()
        .required(),
    client_id: joi
        .string()
        .required(),
    client_secret: joi
        .string()
        .required(),
    username: joi
        .string()
        .optional(),
    password: joi
        .string()
        .optional(),
    refresh_token: joi
        .string()
        .optional()
});

const TOKEN_TTL_SC = 10 * 60 * 60; // 10h
const REFRESH_TOKEN_TTL_SC = 7 * 24 * 60 * 60; // 7d
const AUTH_GRANT_TYPES = {
    PASSWORD: 'password',
    REFRESH_TOKEN: 'refresh_token',
};

export class GetOauthtokenInteractor extends Interactor {

    private _oauthClientGateway: OAuthClientGateway = null;
    private _oauthTokenGateway: OAuthtokenGateway = null;
    private _oauthRefreshTokenGateway: OauthrefreshtokenGateway = null;

    setOAuthClientGateway(gateway: OAuthClientGateway) {
        this._oauthClientGateway = gateway;
        return this;
    }

    setOAuthTokenGateway(gateway: OAuthtokenGateway) {
        this._oauthTokenGateway = gateway;
        return this;
    }

    setOAuthRefreshTokenGateway(gateway: OauthrefreshtokenGateway) {
        this._oauthRefreshTokenGateway = gateway;
        return this;
    }

    async execute(request: GetOAuthTokenRequestObject) {
        try {
            const oauthClient = await this._oauthClientGateway.validateOAuthClient(request.client_id, request.client_secret, request.grant_type);
            if (!oauthClient) {
                return this.presenter.error([ 'OAuth client not found' ]);
            }

            const authService = new AuthService();
            let user;
            if (request.grant_type === AUTH_GRANT_TYPES.PASSWORD) {
                user = await authService.login(request.username, request.password);
            } else {
                user = await authService.loginRefreshToken(request.refresh_token);
            }

            if (!user) {
                return this.presenter.error([ 'User not found' ]);
            }

            const oauthToken = new OAuthToken();
            oauthToken.setOAuthClientId(oauthClient._id);
            oauthToken.setUserId(user._id);
            oauthToken.setAccessToken(uuidv4());
            const dt = new Date();
            dt.setSeconds( dt.getSeconds() + TOKEN_TTL_SC);
            oauthToken.setAccessTokenExpiresAt(dt);

            const { accessToken } = await this._oauthTokenGateway.createOAuthToken(oauthToken);

            const oauthRefreshToken = new OAuthRefreshToken();
            oauthRefreshToken.setOAuthClientId(oauthClient._id);
            oauthRefreshToken.setUserId(user._id);
            oauthRefreshToken.setRefreshToken(uuidv4());
            const dtr = new Date();
            dtr.setSeconds( dtr.getSeconds() + REFRESH_TOKEN_TTL_SC);
            oauthRefreshToken.setRefreshTokenExpiresAt(dtr);

            const { refreshToken } = await this._oauthRefreshTokenGateway.createOAuthRefreshToken(oauthRefreshToken);

            const result = {
                access_token: accessToken,
                expires_in: TOKEN_TTL_SC,
                refresh_token: refreshToken,
            }
            return this.presenter.success(result);
        } catch (e) {
            logger.error('Failed to get access token', request, e);
            return this.presenter.error([e.message]);
        }
    }
}