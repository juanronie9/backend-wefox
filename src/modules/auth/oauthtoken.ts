import { OAuthtokenModel } from '../../db/models/oauthtoken.model';
import { IPersistableEntity, PersistableEntity } from '../common';

export interface IOAuthToken extends IPersistableEntity {
    accessToken: string,
    accessTokenExpiresAt: Date;
    oauthClientId: KeyValueHash;
    userId: KeyValueHash;
}

export class OAuthToken extends PersistableEntity implements IOAuthToken {
    private _accessToken: string;
    private _accessTokenExpiresAt: Date;
    private _oauthClientId: KeyValueHash;
    private _userId: KeyValueHash;

    get accessToken(): string {
        return this._accessToken;
    }

    setAccessToken(value: string): OAuthToken {
        this._accessToken = value;
        return this;
    }

    get accessTokenExpiresAt(): Date {
        return this._accessTokenExpiresAt;
    }

    setAccessTokenExpiresAt(value: Date): OAuthToken {
        this._accessTokenExpiresAt = value;
        return this;
    }

    get oauthClientId(): KeyValueHash {
        return this._oauthClientId;
    }

    setOAuthClientId(value): OAuthToken {
        this._oauthClientId = value;
        return this;
    }

    get userId(): KeyValueHash {
        return this._userId;
    }

    setUserId(value): OAuthToken {
        this._userId = value;
        return this;
    }

    toJSON() {
        const base = super.toJSON();

        return {
            ...base,
            accessToken: this._accessToken,
            accessTokenExpiresAt: this._accessTokenExpiresAt,
            oauthClientId: this._oauthClientId,
            userId: this._userId,
        };
    }

    toModelValues() {
        const raw = this.toJSON();

        for (const key of Object.keys(raw)) {
            if (!raw[key]) delete raw[key];
        }
        return raw;
    }

    static fromRaw(
        raw: KeyValueHash | typeof OAuthtokenModel | any,
        options?: KeyValueHash,
    ): OAuthToken {
        const instance = new this();

        instance
            .setId(raw.id)
            .setName(raw.name)
            .setCreatedAt(raw.createdAt)
            .setUpdatedAt(raw.updatedAt)
            .setAccessToken(raw.accessToken)
            .setAccessTokenExpiresAt(raw.accessTokenExpiresAt)
            .setOAuthClientId(raw.oauthClientId)
            .setUserId(raw.userId)

        return instance;
    }
}
