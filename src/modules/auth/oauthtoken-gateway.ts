import { IOAuthTokenModel, OAuthtokenModel } from '../../db/models/oauthtoken.model';
import { MongoGateway } from '../common/persistance-gateway';
import { OAuthToken } from './oauthtoken';

export class OAuthtokenGateway extends MongoGateway {
    private _oauthTokenModel = OAuthtokenModel;

    setOAuthTokenModel(model: typeof OAuthtokenModel) {
        this._oauthTokenModel = model;
        return this;
    }

    async createOAuthToken(oauthToken: OAuthToken): Promise<OAuthToken> {
        const modelValues = {
            ...oauthToken.toModelValues(),
            createdAt: Date.now(),
            updatedAt: Date.now(),
        }

        const created = await this._oauthTokenModel.create(modelValues);

        return OAuthToken.fromRaw(created);
    }

    async validateAccessToken(token: string): Promise<IOAuthTokenModel> {
        const query = {
            accessToken: token,
            accessTokenExpiresAt: { $gte: new Date() }
        };

        const accessToken = await this._oauthTokenModel
            .findOne(query)
            .exec();

        if (!accessToken) {
            return null;
        }

        return accessToken;
    }
}
