import { OAuthclientModel } from '../../db/models/oauthclient.model';
import { IPersistableEntity, PersistableEntity } from '../common';

export interface IOAuthClient extends IPersistableEntity {
    clientId: string;
    clientSecret: string;
    redirectUris: string[];
    allowedGrants: string[];
}

export class OAuthclient extends PersistableEntity implements IOAuthClient {
    private _clientId: string;
    private _clientSecret: string;
    private _redirectUris: string[];
    private _allowedGrants: string[];

    get clientId(): string {
        return this._clientId;
    }

    setClientId(value: string): OAuthclient {
        this._clientId = value;
        return this;
    }

    get clientSecret(): string {
        return this._clientSecret;
    }

    setClientSecret(value: string): OAuthclient {
        this._clientSecret = value;
        return this;
    }

    get redirectUris(): string[] {
        return [...this._redirectUris];
    }

    setRedirectUris(values: string[]): OAuthclient {
        this._redirectUris = values ? values : [];
        return this;
    }

    get allowedGrants(): string[] {
        return [...this._allowedGrants];
    }

    setAllowedGrants(values: string[]): OAuthclient {
        this._allowedGrants = values ? values : [];
        return this;
    }

    toJSON() {
        const base = super.toJSON();

        return {
            ...base,
            clientId: this._clientId,
            clientSecret: this._clientSecret,
            redirectUris: this._redirectUris,
            allowedGrants: this._allowedGrants,
        };
    }

    toModelValues() {
        const raw = this.toJSON();

        for (const key of Object.keys(raw)) {
            if (!raw[key]) delete raw[key];
        }
        return raw;
    }

    static fromRaw(
        raw: KeyValueHash | typeof OAuthclientModel | any,
        options?: KeyValueHash,
    ): OAuthclient {
        const instance = new this();

        instance
            .setId(raw.id)
            .setName(raw.name)
            .setCreatedAt(raw.createdAt)
            .setUpdatedAt(raw.updatedAt)
            .setClientId(raw.clientId)
            .setClientSecret(raw.clientSecret)
            .setRedirectUris(raw.redirectUris)
            .setAllowedGrants(raw.allowedGrants)

        return instance;
    }
}
