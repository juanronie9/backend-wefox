import mongoose, { Document } from 'mongoose';
import { baseEntitySchema } from './base-entity-schema';

export interface IOAuthClientModel extends Document {
    _id: string;
    name: string;
    clientId: string;
    clientSecret: string;
    redirectUris: string[],
    allowedGrants: string[],
    createdAt: Date;
    updatedAt: Date;
}

export const oauthClientSchema = new mongoose.Schema(
    {
        ...baseEntitySchema,
        clientId: String,
        clientSecret: String,
        redirectUris: [String],
        allowedGrants: [String],
    },
    { timestamps: true },
);

export const OAuthclientModel = mongoose.model<IOAuthClientModel>('OAuthClient', oauthClientSchema);
