import mongoose, { Document, Schema } from 'mongoose';
import { baseEntitySchema } from './base-entity-schema';

export interface IOAuthRefreshTokenModel extends Document {
    _id: string;
    name: string;
    refreshToken: string;
    refreshTokenExpiresAt: Date;
    oauthClientId: string;
    userId: string;
    scopes: string[];
    createdAt: Date;
    updatedAt: Date;
}

export const oauthTokenSchema = new mongoose.Schema(
    {
        ...baseEntitySchema,
        refreshToken: String,
        refreshTokenExpiresAt: Date,
        oauthClientId: { type: Schema.Types.ObjectId, ref: 'OAuthClient' },
        userId: { type: Schema.Types.ObjectId, ref: 'User' },
        scopes: [String],
    },
    { timestamps: true },
);

export const OAuthRefreshTokenModel = mongoose.model<IOAuthRefreshTokenModel>('OAuthRefreshToken', oauthTokenSchema);
